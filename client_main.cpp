#include <iostream>
#include <exception>
#include "src/client/client.hpp"
#include "src/log.hpp"

using namespace std;
using namespace raggin::client;


int main()
try
{
	init_log();
	client client(1337);
	cin.get();
}
catch(exception& ex)
{
	cerr << ex.what() << endl;
	cin.get();
}