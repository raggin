#include <iostream>
#include <exception>
#include <vector>
#include <string>
#include <list>
#include <boost/asio.hpp>
// #include "src/socket.hpp"
#include "src/log.hpp"
// #include "src/session_container.hpp"
// #include "src/server/session.hpp"
#include <boost/shared_ptr.hpp>
#include <boost/uuid.hpp>
#include <boost/enable_shared_from_this.hpp>

using namespace std;
using namespace boost;
using namespace boost::asio::ip;
//using namespace raggin;

class Y;
typedef shared_ptr<Y> Yptr;

class Y : public enable_shared_from_this<Y>
{
public:
};




int main()
try
{
	init_log();

	asio::io_service io_service;

	Yptr yp(new Y());

	Yptr ptr =  yp->shared_from_this();

	/*
	asio::io_service io_service;

	raggin::socket socket(io_service);
	socket.accept(1337);
	socket.send(asio::buffer("test!"));

	vector<char> vecbuf(4);
	socket.receive(asio::buffer(vecbuf));
	cout << string(vecbuf.begin(), vecbuf.end()) << endl;
	*/

	cin.get();
}
catch(exception& ex)
{
	cerr << ex.what() << endl;
	cin.get();
}