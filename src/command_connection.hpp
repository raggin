#ifndef command_connection_hpp_INCLUDED
#define command_connection_hpp_INCLUDED

#include <boost/asio.hpp>
#include <boost/shared_ptr.hpp>
#include "connection.hpp"


namespace raggin
{
	class command_connection : public connection
	{
	public:
		explicit command_connection(boost::asio::io_service& io_service);

		command_connection(connection& conn);
	};

	typedef boost::shared_ptr<command_connection> command_connection_ptr;
}

#endif