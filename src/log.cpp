#include "log.hpp"
#include <boost/logging/format.hpp>
#include <boost/logging/writer/ts_write.hpp>

using namespace boost::logging;


BOOST_DEFINE_LOG_FILTER(g_log_filter, filter::no_ts)
BOOST_DEFINE_LOG(g_l, log_type) 

void init_log() 
{
	//g_l->writer().add_formatter( formatter::idx() );
	g_l->writer().add_formatter( formatter::time("[$hh:$mm:$ss] ") );
	g_l->writer().add_formatter( formatter::append_newline() );

	g_l->writer().add_destination( destination::cout() );
	g_l->writer().add_destination( destination::dbg_window() );
}