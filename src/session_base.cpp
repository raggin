#include "session_base.hpp"
#include "core_module.hpp"


raggin::session_base::session_base(command_connection& cmd_conn)
	:modules_(*this)
	,cmd_conn_(cmd_conn)
{
	modules_.add<core_module>();
}

raggin::command_connection& raggin::session_base::cmd_conn()
{
	return cmd_conn_;
}

raggin::data_connection_container_ptr raggin::session_base::data_conns()
{
	return data_conns_ptr_;
}

const raggin::session_id raggin::session_base::id() const
{
	return id_;
}