#include "session_container.hpp"
#include <boost/lambda/lambda.hpp>
#include <boost/lambda/bind.hpp>
#include <boost/bind.hpp>

using namespace std;
using namespace boost;


void raggin::session_container::insert(session_base_ptr session_ptr)
{
	push_back(session_ptr);
}

raggin::session_base_ptr raggin::session_container::get(session_id session_id)
{
	iterator itr = std::find_if(begin(), end(), (boost::bind(&session_base::id, _1) == session_id));
	if(itr != end())
		return *itr;
	return session_base_ptr();
}