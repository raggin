#ifndef log_hpp_INCLUDED
#define log_hpp_INCLUDED

#include <boost/logging/format_fwd.hpp>


typedef boost::logging::logger_format_write< 
	boost::logging::default_, 
	boost::logging::default_, 
	boost::logging::writer::threading::ts_write 
> log_type;

BOOST_DECLARE_LOG_FILTER(g_log_filter, boost::logging::filter::no_ts ) 
BOOST_DECLARE_LOG(g_l, log_type) 

#define LOG BOOST_LOG_USE_LOG_IF_FILTER(g_l, g_log_filter->is_enabled())

void init_log();

#endif
