#include "command.hpp"

using namespace std;


raggin::command::command()
	:name_("NoCommand")	
{
}

raggin::command::command(string cmd_name)
	:name_(cmd_name) 
{
}

raggin::command::command(string cmd_name, param_vector params)
	:name_(cmd_name)
	,params_(params)
{
}
