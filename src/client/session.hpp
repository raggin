#ifndef session_hpp_INCLUDED
#define session_hpp_INCLUDED

#include <boost/shared_ptr.hpp>
#include <boost/asio.hpp>
#include "../command_connection.hpp"
#include "../session_base.hpp"


namespace raggin
{
	namespace client
	{
		class session : public session_base
		{
		public:
			session(command_connection& cmd_conn)
				:session_base(cmd_conn)
			{
			}

			virtual void start()
			{
			}
		};

		typedef boost::shared_ptr<session> session_ptr;
	}
}

#endif