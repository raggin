#include "client.hpp"
#include "session.hpp"
#include "../log.hpp"
#include "../session_id.hpp"
#include "../session_base.hpp"
#include "../connection.hpp"
#include <boost/bind.hpp>
#include <boost/thread.hpp>
#include <vector>

using namespace std;
using namespace boost;


raggin::client::client::client(uint16_t accept_port)
	:accept_port_(accept_port)
{
	async_accept();
	thread t(boost::bind(&asio::io_service::run, &io_service_));
}

void raggin::client::client::async_accept()
{
	connection_ptr conn_ptr(new connection(io_service_));
	conn_ptr->socket()->async_accept(accept_port_, 
		bind(&client::handle_async_accept, this, _1, conn_ptr));
}

void raggin::client::client::handle_async_accept(const boost::system::error_code& error, connection_ptr conn_ptr)
{
	if(!error)
	{
		/**	receive session_id from other side
		 */
		vector<char> inbound_buf(36);
		conn_ptr->socket()->receive(asio::buffer(inbound_buf));
		session_id session_id(inbound_buf.begin(), inbound_buf.end());

		/**	if there is already a session with that session_id,
		 *		add the connection as a data-connection,
		 *	otherwise create new session
		 */
		session_base_ptr existing_session = sessions_.get(session_id);
		if(existing_session)
		{
			data_connection_ptr data_conn(new data_connection(*conn_ptr, existing_session->cmd_conn()));
			existing_session->data_conns()->insert(data_conn);
		}
		else
		{
 			//command_connection_ptr cmd_conn_ptr(new command_connection(conn_ptr));
 			session_ptr new_session(new session(command_connection(*conn_ptr)));
			sessions_.insert(new_session);
			LOG << "Server accepted.";
		}

		async_accept();
	}
	else
	{
		LOG << "client.handle_async_accept: " << error.message();
	}	
}