#ifndef client_hpp_INCLUDED
#define client_hpp_INCLUDED

#include <boost/cstdint.hpp>
#include <boost/asio.hpp>
#include "../socket.hpp"
#include "../session_container.hpp"


namespace raggin
{
	namespace client
	{
		class client
		{
			boost::asio::io_service io_service_;
			boost::uint16_t accept_port_;
			session_container sessions_;

			void async_accept();
			void handle_async_accept(const boost::system::error_code& error, connection_ptr conn_ptr);

		public:
			/** Constructor.
			 *	@param port the port on which to listen for new connections
			 */
			client(boost::uint16_t port);
		};
	}
}

#endif