#ifndef connection_hpp_INCLUDED
#define connection_hpp_INCLUDED

#include <boost/asio.hpp>
#include <boost/shared_ptr.hpp>
#include "socket.hpp"


namespace raggin
{
	class connection
	{
	public:
		explicit connection(boost::asio::io_service& io_service);
		//explicit connection(socket_ptr socket_ptr);

		socket_ptr socket();

	private:
		socket_ptr socket_ptr_;
	};

	typedef boost::shared_ptr<connection> connection_ptr;
}

#endif