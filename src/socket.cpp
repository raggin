#include "socket.hpp"
#include "Log.hpp"
#include <boost/bind.hpp>

using namespace std;
using namespace boost;
using namespace boost::asio;
using namespace boost::asio::ip;


raggin::socket::socket(asio::io_service& io_service)
	:tcp::socket(io_service)
{
}

raggin::socket::socket(socket_ptr other_socket_ptr)
	:tcp::socket(other_socket_ptr->get_io_service(), tcp::v4(), other_socket_ptr->native())
	,other_socket_ptr_(other_socket_ptr)
{
}

raggin::socket::~socket()
{
}

void raggin::socket::close()
{
	close();
}

void raggin::socket::accept(uint16_t port)
{
	acceptor_ptr_.reset(new tcp::acceptor(get_io_service(), tcp::endpoint(tcp::v4(), port)));
	try
	{
		acceptor_ptr_->accept(static_cast<boost::asio::ip::tcp::socket&>(*this));
		acceptor_ptr_.reset();
	}
	catch(...)
	{
		acceptor_ptr_.reset();
		throw;
	}
}

void raggin::socket::async_accept(uint16_t port, accept_handler handler)
{
	acceptor_ptr_.reset(new tcp::acceptor(get_io_service(), tcp::endpoint(tcp::v4(),port)));
	acceptor_ptr_->async_accept(static_cast<boost::asio::ip::tcp::socket&>(*this), 
		boost::bind(&socket::handle_async_accept, this, _1, handler));
}

void raggin::socket::handle_async_accept(const boost::system::error_code& error, accept_handler handler)
{
	LOG << "socket.handle_async_accept: " << (error?"error":"no error");
	acceptor_ptr_.reset();
	handler(error);
}

void raggin::socket::connect(const string& ip, uint16_t port)
{
	tcp::endpoint endpoint(address().from_string(ip), port);
	tcp::socket::connect(endpoint);
}

void raggin::socket::async_connect(tcp::endpoint endpoint, connect_handler handler)
{
	LOG << "socket.async_connect";
	tcp::socket::async_connect(endpoint, handler);	
}

void raggin::socket::async_connect(const string& ip, uint16_t port, connect_handler handler)
{
	tcp::endpoint endpoint(address::from_string(ip), port);
	async_connect(endpoint, handler);
}


boost::asio::ip::tcp::endpoint raggin::socket::local_endpoint() const
{ 
	return tcp::socket::local_endpoint(); 
}

boost::asio::ip::tcp::endpoint raggin::socket::remote_endpoint() const
{ 
	return tcp::socket::remote_endpoint(); 
}