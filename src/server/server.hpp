#ifndef server_hpp_INCLUDED
#define server_hpp_INCLUDED

#include <string>
#include <boost/asio.hpp>
#include <boost/cstdint.hpp>
#include "session.hpp"


namespace raggin
{
	namespace server
	{
		class server
		{
			static const int connect_interval = 10;	//Sekunden
			boost::asio::io_service io_service_;
			boost::asio::deadline_timer timer_;
			boost::asio::ip::tcp::endpoint remote_endpoint_;	//Beinhaltet den Endpoint zum Connecten.
			session_ptr session_ptr_;

			void async_connect();	//Zu Client
			void handle_async_connect(const boost::system::error_code& error, connection_ptr conn_ptr);

		public:
			server(const std::string& ip, boost::uint16_t port);
			void run();
		};
	}
}

#endif
