#include "server.hpp"
#include "../log.hpp"
#include <boost/bind.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

using namespace std;
using namespace boost;
using namespace boost::asio;


raggin::server::server::server(const string& ip, boost::uint16_t port)
	:remote_endpoint_(ip::address::from_string(ip), port)
	,timer_(io_service_)
{
}

void raggin::server::server::run()
{
	async_connect();
	io_service_.run();
}

void raggin::server::server::async_connect()
{
	LOG << "Connecting..";
	connection_ptr conn_ptr(new connection(io_service_));
	conn_ptr->socket()->async_connect(remote_endpoint_,
		bind(&server::handle_async_connect, this, _1, conn_ptr));
}

void raggin::server::server::handle_async_connect(const boost::system::error_code& error, connection_ptr conn_ptr)
{
	if(!error)
	{
		session_ptr_.reset(new session(command_connection(*conn_ptr)));
		LOG << "Connection to Client etablished.";
		session_ptr_->start();
		//session_ptr->on_disconnected( boost::bind(&server::SessionDisconnectedHandler, this) );
	}
	else
	{
		LOG << error.message() << " (Next attempt in " << connect_interval << " seconds)";
		conn_ptr.reset();
		timer_.expires_from_now(posix_time::seconds(connect_interval));
		timer_.async_wait( boost::bind(&server::async_connect, this) );
	}
}
