#ifndef session_base_hpp_INCLUDED
#define session_base_hpp_INCLUDED

#include <string>
#include <boost/shared_ptr.hpp>
#include "module_container.hpp"
#include "command_connection.hpp"
#include "data_connection_container.hpp"
#include "session_id.hpp"


namespace raggin
{
	class session_base
	{
		module_container modules_;
		command_connection& cmd_conn_;
		data_connection_container_ptr data_conns_ptr_;
		const session_id id_;

	public:
		session_base(command_connection& cmd_conn);

		//virtual void start()=0;

		command_connection& cmd_conn();
		data_connection_container_ptr data_conns();
		const session_id id() const;
	};

	typedef boost::shared_ptr<session_base> session_base_ptr;
}

#endif