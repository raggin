#ifndef socket_hpp_INCLUDED
#define socket_hpp_INCLUDED

#include <boost/shared_ptr.hpp>
#include <boost/asio.hpp>
#include <boost/cstdint.hpp>
#include <string>


namespace raggin
{
	class socket;
	typedef boost::shared_ptr<socket> socket_ptr;

	class socket : public boost::asio::ip::tcp::socket
	{
		typedef boost::shared_ptr<boost::asio::ip::tcp::acceptor> acceptor_ptr;

		/**	handler types for the asynchronous socket operations.
		 */
		typedef boost::function<void(const boost::system::error_code&)> connect_handler;
		typedef connect_handler accept_handler;
		typedef boost::function<void(const boost::system::error_code&, std::size_t)> send_handler;
		typedef connect_handler receive_handler;

		/**	Wenn Socket ueber einen einbestehenden Socket_ptr erstellt wird,
		 *	speichern wir den Socket_ptr, damit der Socket am leben bleibt.
		 */
		socket_ptr other_socket_ptr_;

		acceptor_ptr acceptor_ptr_;

		/**	internal accept handler.
		 *	to reset the acceptor_ptr after an accepting operation.
		 */
		void handle_async_accept(const boost::system::error_code& error, accept_handler handler);

	public:
		socket(boost::asio::io_service& io_service);
		socket(socket_ptr other_socket_ptr);
		virtual ~socket();

		//boost::asio::io_service& get_io_service();

		void close();

		void accept(boost::uint16_t port);
		void async_accept(boost::uint16_t port, accept_handler handler);
		void connect(const std::string& host, boost::uint16_t port);
		void async_connect(boost::asio::ip::tcp::endpoint endpoint, connect_handler handler);
		void async_connect(const std::string& ip, boost::uint16_t port, connect_handler handler);

		/**	receive.
		 */
		template<typename MutableBufferSequence>
		std::size_t receive(const MutableBufferSequence& buffers);

		/**	send.
		 */
		template<typename ConstBufferSequence> 
		std::size_t send(const ConstBufferSequence& buffers);

		/**	async_receive.
		 */
		template<typename MutableBufferSequence>
		void async_receive(const MutableBufferSequence& buffers, receive_handler handler);

		/**	async_send.
		 */
		template<typename ConstBufferSequence>
		void async_send(const ConstBufferSequence& buffers, send_handler handler);

		boost::asio::ip::tcp::endpoint local_endpoint() const;
		boost::asio::ip::tcp::endpoint remote_endpoint() const;
	};


	template<typename MutableBufferSequence>
	std::size_t socket::receive(const MutableBufferSequence& buffers)
	{
		return boost::asio::read(static_cast<boost::asio::ip::tcp::socket&>(*this), buffers);
	}

	template<typename ConstBufferSequence> 
	std::size_t socket::send(const ConstBufferSequence& buffers)
	{
		return boost::asio::write(static_cast<boost::asio::ip::tcp::socket&>(*this), buffers);
	}

	template<typename MutableBufferSequence>
	void socket::async_receive(const MutableBufferSequence& buffers, receive_handler handler)
	{
		boost::asio::async_read(static_cast<boost::asio::ip::tcp::socket&>(*this), buffers, handler);
	}

	template<typename ConstBufferSequence>
	void socket::async_send(const ConstBufferSequence& buffers, send_handler handler)
	{
		boost::asio::async_write(static_cast<boost::asio::ip::tcp::socket&>(*this), buffers, handler);
	}
}

#endif
