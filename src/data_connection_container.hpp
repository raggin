#ifndef data_connection_container_hpp_INCLUDED
#define data_connection_container_hpp_INCLUDED

#include <vector>
#include "data_connection.hpp"

namespace raggin
{
	class data_connection_container : private std::vector<data_connection_ptr>
	{
	public:
		void insert(data_connection_ptr dara_conn_ptr);
	};

	typedef boost::shared_ptr<data_connection_container> data_connection_container_ptr;
}

#endif