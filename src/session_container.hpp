#ifndef session_container_hpp_INCLUDED
#define session_container_hpp_INCLUDED

#include <vector>
#include "session_base.hpp"
#include "session_id.hpp"


namespace raggin
{
	class session_container : private std::vector<session_base_ptr>
	{
	public:
		/**	Inserts session_ptr to the container.
		 */
		void insert(session_base_ptr session_ptr);

		/**

		/**	Get session_base_ptr by session_id.
		 */
		session_base_ptr get(session_id session_id);
	};
}

#endif