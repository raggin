#include "data_connection.hpp"


raggin::data_connection::data_connection(boost::asio::io_service& io_service, command_connection& cmd_conn)
	:connection(io_service)
	,cmd_conn_(cmd_conn)
{
}

raggin::data_connection::data_connection(connection& conn, command_connection& cmd_conn)
	:connection(conn)
	,cmd_conn_(cmd_conn)
{
}