#include "command_connection.hpp"


raggin::command_connection::command_connection( boost::asio::io_service& io_service )
	:connection(io_service)
{
}

raggin::command_connection::command_connection( connection& conn )
	:connection(conn)
{
}