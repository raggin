#ifndef param_vector_hpp_INCLUDED
#define param_vector_hpp_INCLUDED

#include <boost/variant.hpp>
#include <string>
#include <vector>


namespace raggin
{
	//Anonymer namespace, damit param_types_variant nicht oeffentlich sichtbar ist
	namespace
	{
		typedef boost::variant
			<	
			std::string,
			int,
			std::vector<unsigned char> 
			> 
			param_types_variant;
	}

	typedef std::vector<param_types_variant> param_vector;
}

#endif