#ifndef session_id_hpp_INCLUDED
#define session_id_hpp_INCLUDED

#include <string>


namespace raggin
{
	struct session_id : std::string
	{
		template<class InputIterator> 
		session_id(InputIterator begin, InputIterator end);

		session_id();
	};
}

template<class InputIterator>
raggin::session_id::session_id(InputIterator begin, InputIterator end)
	:std::string(begin, end)
{
}

#endif