#ifndef data_hpp_INCLUDED
#define data_hpp_INCLUDED

#include <boost/shared_ptr.hpp>
#include <string>
#include "command.hpp"


namespace raggin
{
	class data : public command
	{
	public:
		data();
		data(std::string cmd_name);
	};
}

#endif