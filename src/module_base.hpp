#ifndef module_base_hpp_INCLUDED
#define module_base_hpp_INCLUDED

#include <boost/shared_ptr.hpp>


namespace raggin
{
	//fwd
	class session_base;

	class module_base
	{
	public:
		module_base(session_base& _session);

	private:
		session_base& session_;
	};

	typedef boost::shared_ptr<module_base> module_base_ptr;
}

#endif