#include "connection.hpp"


raggin::connection::connection( boost::asio::io_service& io_service )
	:socket_ptr_(new raggin::socket(io_service))
{
}

// raggin::connection::connection( socket_ptr socket_ptr )
// 	:socket_ptr_(socket_ptr)
// {
// }

raggin::socket_ptr raggin::connection::socket()
{
	return socket_ptr_;
}
