#ifndef core_module_hpp_INCLUDED
#define core_module_hpp_INCLUDED

#include <string>
#include "module_base.hpp"
#include "param_vector.hpp"


namespace raggin
{
	//fwd
	class session_base;

	/**	Das Hauptmodul.
	 *	Stellt grundlegenes zur Verfuegung und ist seitenunabhaengig. 
	 */
	class core_module : public module_base
	{
		void log_(const param_vector& params);
		void error_(const param_vector& params);

	public:
		static const std::string	
			log, 
			error;

		core_module(session_base& _session);

		//ModuleManager::ModuleFunctionMap FunctionMap();
	};
}

#endif