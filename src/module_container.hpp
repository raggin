#ifndef module_manager_hpp_INCLUDED
#define module_manager_hpp_INCLUDED

#include <boost/static_assert.hpp>
#include <boost/shared_ptr.hpp>
#include "module_base.hpp"
#include "command.hpp"
#include "data.hpp"


namespace raggin
{
	//fwd
	class session_base;

	class module_container
	{
	public:
		module_container(session_base& session);

		/**	Registers a module.
		 */
		template<typename ModuleType> 
			void add();
		
		/**	Returns registered module with type of ModuleType.
		 */
		template<typename ModuleType> 
			boost::shared_ptr<ModuleType> get();

// 		void process_cmd(command& cmd);
// 		void process_data(data& data);

	private:
		session_base& session_;
		std::vector<module_base_ptr> modules_;
	};
}

template<typename ModuleType>
void raggin::module_container::add()
{
	BOOST_STATIC_ASSERT((boost::is_convertible<ModuleType, module_base>::value));

	module_base_ptr module(new ModuleType(session_));
	modules_.push_back(module);
}

template<typename ModuleType> 
boost::shared_ptr<ModuleType> raggin::module_container::get()
{
	BOOST_STATIC_ASSERT((boost::is_convertible<ModuleType, module_base>::value));

	typedef std::vector<module_base_ptr> iterator;
	iterator itr = std::find_if(modules_.begin(), modules_.end(), 
		(typeid(*_1) == typeid(ModuleType)));

	if(itr == modules_.end())
		throw runtime_error("Module of type" + typeid(ModuleType).name() + "is not registered.")

}

#endif