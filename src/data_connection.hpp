#ifndef data_connection_hpp_INCLUDED
#define data_connection_hpp_INCLUDED

#include <boost/asio.hpp>
#include <boost/shared_ptr.hpp>
#include "connection.hpp"
#include "command_connection.hpp"


namespace raggin
{
	class data_connection : public connection
	{
	public:
		data_connection(boost::asio::io_service& io_service, command_connection& cmd_conn);

		/**	Constructor.
		 *	@param conn connection which will be used for this data_connection.
		 */
		data_connection(connection& conn, command_connection& cmd_conn);

	private:
		command_connection& cmd_conn_;
	};

	typedef boost::shared_ptr<data_connection> data_connection_ptr;
}

#endif