#ifndef command_hpp_INCLUDED
#define command_hpp_INCLUDED

#include <string>
#include "param_vector.hpp"


namespace raggin
{
	class command
	{
	public:
		/**	DefaultKonstruktor.
		*/
		command();

		/**	Konstruktor.
		*	@param cmdName Der Name des Commands.
		*/
		command(std::string cmd_name);

		/**	Konstruktor.
		*	@param cmdName Der Name des Commands.
		*	@param params Die Parameter des Commands.
		*/
		command(std::string cmd_name, param_vector params);

		/**	Gibt einen Parameter zurueck.
		*	@param index Index des Parameters.
		*/
		template<typename T> 
		T & get_param(int index) 
		{
			return boost::get<T>(params_.at(index));
		}

		/**	Fuegt einen Parameter hinzu.
		*	@param newParam Objekt, dass als Parameter hinzugefuegt werden soll.
		*/
		template<typename T> 
		void add_param(const T & param)
		{
			params_.push_back(param);
		}

	private:
		std::string name_;
		param_vector params_;
	};
}

#endif