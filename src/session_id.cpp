#include "session_id.hpp"
#include <boost/uuid.hpp>


raggin::session_id::session_id()
	:std::string( boost::uuid::create().to_string() ) 
{
}