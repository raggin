#include <iostream>
#include <exception>
#include "src/server/server.hpp"
#include "src/log.hpp"

using namespace std;


int main()
try
{
	using namespace raggin::server;

	init_log();

	server srv("127.0.0.1", 1337);
	srv.run();

	cin.get();
}
catch(exception& ex)
{
	cerr << ex.what() << endl;
	cin.get();
}